# Udovyk Liliia

## Task 1
![](http://i.piccy.info/i9/4d890caa40a7da2d8029037f8be56810/1574110916/37936/1346751/01.jpg)
```js
    let ticketPrice = 220,
        numberTickets = 3,
        totalСost = ticketPrice * numberTickets,
        cash = 0,
        permittedValue = 100,
        enteredValue = 0,
        transactionCount = 0;
    for (cash; cash <= totalСost; cash += enteredValue) {
        enteredValue = Number(prompt("Enter withdrawal amount"));
        if (enteredValue !== permittedValue) {
            enteredValue = 0;
            alert(`It is possible to withdraw only ${permittedValue} UAH per transaction`);
        } else {
            transactionCount++;
        }
    }
    if (cash >= totalСost) {
        console.log(`You withdrew ${cash} UAH`);
        console.log(`You have completed ${transactionCount} transactions`);
        console.log("You can buy tickets"); 
    }
```

## Task 2
![](http://i.piccy.info/i9/d3b6e683b0c334c9d17c9b6912bf230a/1574110994/25720/1346751/02.jpg)
```js
    function validation(paramName, validLength) {
        let value;
        do {
            value = prompt(`Enter your ${paramName} (minimum ${validLength} characters)`)
        } while(typeof value === "string" && value.trim().length < validLength);
        return value;
    }
    
    function authorization() {
        let values = [
            { paramName: "name",
            validLength: 3 },
            { paramName: "password",
            validLength: 6 }
        ];
        let value;
        for (value in values) {
            let result = validation(values[value].paramName, values[value].validLength);
            if (!result) {
                return alert("Operation was canceled")
            }
        }
        alert("Your authorization was successful!");
    }
    authorization();
```

