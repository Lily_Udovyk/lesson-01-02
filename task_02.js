function validation(paramName, validLength) {
  let value;
  do {
    value = prompt(`Enter your ${paramName} (minimum ${validLength} characters)`)
  } while(typeof value === "string" && value.trim().length < validLength);
  return value;
}
  
function authorization() {
  let values = [
    { paramName: "name",
      validLength: 3 },
    { paramName: "password",
      validLength: 6 }
  ];
  let value;
  for (value in values) {
    let result = validation(values[value].paramName, values[value].validLength);
    if (!result) {
			return alert("Operation was canceled")
		}
  }
  alert("Your authorization was successful!");
}
authorization();